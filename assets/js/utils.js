//-----------------------------//
//------- Browser utils -------//
//-----------------------------//

function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

// Internal router
function goTo(viewName){
    // Clear previous storage
    clearStorage()
    // Store state in session storage
    storeState()
    window.location.href = host + viewName + ".html"
}

function clearStorage() {
    sessionStorage.clear()
}

// Here, blob refers to the files blob, not the shape
function downloadBlob(blob, filename) {
    // Create an object URL for the blob object
    const url = URL.createObjectURL(blob)
    
    // Create a new anchor element
    const a = document.createElement('a')
    
    // Set the href and download attributes for the anchor element
    a.href = url
    a.download = filename || 'download'
    
    // Click handler that releases the object URL after the element has been clicked
    // This is required for one-off downloads of the blob content
    const clickHandler = () => {
      setTimeout(() => {
        URL.revokeObjectURL(url)
        this.removeEventListener('click', clickHandler)
      }, 150)
    };
    
    // Add the click event listener on the anchor element
    // Comment out this line if you don't want a one-off download of the blob content
    a.addEventListener('click', clickHandler, false)
    
    // Programmatically trigger a click on the anchor element
    a.click()
    
    document.removeChild(a)
}

function copyTextToClipboard(text) {
    navigator.clipboard.writeText(text); 
}


//-----------------------------//
//--------- Math utils --------//
//-----------------------------//

const PI = 3.1415

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); // The maximum is exclusive and the minimum is inclusive
}

function generatePoint(center, radius){
    return [center[0] + getRandomInt(-radius,radius), center[1] + getRandomInt(-radius,radius)]
}

function mod(v) {
    return Math.sqrt(v[0]*v[0]+v[1]*v[1]) // Teorema de pitágoras
}

function rotatePoint(point, angle, center = [0,0]) {
    let v = [point[0]-center[0],point[1]-center[1]]
    let module = mod(v)
    let alpha = Math.acos(v[0]/module)
    let rotatedV = [Math.round(module*Math.cos((angle*PI/180)+alpha)),Math.round(module*Math.sin((angle*PI/180)+alpha))]
    return [rotatedV[0]+center[0],rotatedV[1]+center[1]]
}

//-----------------------------//
//------ Libre blob utils -----//
//-----------------------------//

function updateConfig(key=null,value=null) {
    
    let config = getConfig()

    // Cast depending on updated key
    switch(key){
        case 'fill':
            config[key] = value
        break
        default:
            config[key] = Number(value)
        break
    }

    // Store new config
    setConfig(config)
    return config
}

function allocateBlob(blobSvg){
    if(typeof blobSvg === 'string'){

        // Create HTML element from parent to be able se outerHTML property
        let svgEl = document.createElement('div')
        svgEl.innerHTML = blobSvg

        blobSvg = svgEl.firstElementChild
    }

    // Set it as background in empty slot if there are available ones
    let slots = Array.from(document.getElementsByClassName('empty-slot'))
    if(slots.length > 0){
        slots[0].classList.remove('empty-slot')
        slots[0].classList.add('small-blob-container')
        slots[0].appendChild(blobSvg)
    }
}

function restoreSession() {
    // Load state from session storage
    restoreState()

    if(getBlobs().length > 0){

        // Draw blobs and bg
        let blobs = getBlobs()
        blobs.forEach(blobObj => {
            allocateBlob(blobObj.svgElString)
        })
    }

    return getConfig()

}

function toggleCodeModal() {
    let modalEl = document.getElementById("svg-code-modal")
    let bodyEl = document.getElementsByTagName("body")[0]
    if(modalEl.open == null){
        // Open modal
        modalEl.open = true
        // Add CSS classes   
        modalEl.classList.add('f-col')
        bodyEl.classList.add('modal-opened')
    } else {
        modalEl.open = !modalEl.open;
        modalEl.classList.toggle('f-col')
        bodyEl.classList.toggle('modal-opened')
    }
}

function downloadSVG(name) {
    let blob
    if(name === 'blob'){
        // The blob referenced here is the Blob object of the browser, which is a superset of a file not the shapes
        blob = new Blob([getRaphaelObj().canvas.outerHTML], {type: 'image/svg+xml'})
    } else if (name === 'blob-background'){
        blob = new Blob([document.getElementById(getCurrentBackground().id).outerHTML], {type: 'image/svg+xml'})
    }
    downloadBlob(blob,name)
}

function copySVGCode() {
    // Get the text field
    let copyText = document.getElementById('svg-code-p').outerHTML;

    // Copy the text inside the text field
    navigator.clipboard.writeText(copyText.value);  
}