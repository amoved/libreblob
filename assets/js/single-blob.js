function clearBg(container) {
    container.innerHTML= ''
}

function getCirclePoints(nPoints,center,radius) {
    let angle = 0
    const deltaAngle = 2*PI/nPoints
    let points = []
    for(let i=0;i<nPoints;i++){
        points[i] = [center[0] + radius * Math.cos(angle), center[1] + radius * Math.sin(angle)]
        angle += deltaAngle
    }
    return points
}

function getRandomCirclePoints(nPoints,center,radius,deviation) {
    let angle = 0
    const deltaAngle = 2*PI/nPoints
    let points = []
    for(let i=0;i<nPoints;i++){
        circlePoint = [center[0] + radius * Math.cos(angle), center[1] + radius * Math.sin(angle)]
        points[i] = generatePoint(circlePoint, deviation)
        angle += deltaAngle
    }
    return points
}

function generateCatmutPathInput(center, numOfPoints, irregularity, radius) {

    let angle = 0
    let points = []
    const deltaAngle = 2*PI/numOfPoints
    const maxDeviation = 20
    const deviationRadius = maxDeviation*irregularity
    const initialPoint = [center[0] + radius, center[1]]
    points.push(initialPoint)
    let path= `M ${initialPoint[0]},${initialPoint[1]} R `
    angle += deltaAngle

    // Calculate intermidiate numOfPoints
    for(let i=1;i < numOfPoints;i++){
        const end = generatePoint([center[0] + radius * Math.cos(angle), center[1] + radius * Math.sin(angle)], deviationRadius)
        points.push(end)
        path += `${end[0].toFixed(0)},${end[1].toFixed(0)} `
        angle += deltaAngle
    }
    path += 'Z'

    return [points,path]
}

function generateCatmutPathInputFromPoints(points) {
    console.log('Points:')
    console.log(points)
    let path= `M ${points[0][0]},${points[0][1]} R `
    for(let i=1;i < points.length;i++){
        path += `${points[i][0].toFixed(0)},${points[i][1].toFixed(0)} `
    }
    path += 'Z'
    return path
}

// Initialize Raphäel object, provided origin and size of the canvas
// and sets html svg attributes
function generateRaphaelObj(x = 0, y = 0, w = 200, h = 200) {
    
    let raphaelObj = new Raphael(x,y,w,h) 

    raphaelObj.canvas.setAttribute('viewBox',`${x} ${y} ${w} ${h}`)
    raphaelObj.canvas.setAttribute('xmlns','http://www.w3.org/2000/svg')
    raphaelObj.canvas.removeAttribute('height')
    raphaelObj.canvas.removeAttribute('width')

    return raphaelObj
}

// Generates a blob shape, configures its html attributes and stores it in state
function createBlob(raphaelObj,{numOfPoints,irregularity, stroke, fill},center=[100,100],baseRadius=50) {
    
    let [points, catmutPathInput] = generateCatmutPathInput(center, numOfPoints, irregularity, baseRadius)
    
    // Generate Path 
    let pathEl = raphaelObj.path(catmutPathInput)

    // Set path HTML element attributes
    pathEl.node.setAttribute('stroke-linecap', 'round')
    pathEl.node.setAttribute('stroke-linejoin', 'round')
    pathEl.node.setAttribute('stroke-width', stroke)
    pathEl.node.setAttribute('fill', fill)
    pathEl.node.setAttribute('class','blob')
    pathEl.node.setAttribute('id',`blob-path-${Date.now()}`)

    return {
        catmutPathInput,
        center: center,
        radius: baseRadius,
        points: [...points],
        angle: 0,
    }

}

function updateSVGPaths() {
    let containerEl = document.getElementsByClassName('blob-container')[0]
    let raphaelObj = getRaphaelObj()
    let blobs = getBlobs()
    let config = getConfig()
    raphaelObj.clear()
    blobs.forEach(blob => {
        let pathEl = raphaelObj.path(blob.catmutPathInput)
        // Set path HTML element attributes
        pathEl.node.setAttribute('stroke-linecap', 'round')
        pathEl.node.setAttribute('stroke-linejoin', 'round')
        pathEl.node.setAttribute('stroke-width', config.stroke)
        pathEl.node.setAttribute('fill', config.fill)

    })
    containerEl.innerHTML = ''
    containerEl.appendChild(raphaelObj.canvas)
}

function magicBlobs(config) {

    // Is there a config set already?
    if(getConfig() !== {}){
        // Check config
        if(!config.targetEl) {
            console.log("Specify the target element to add the background")
        }
        // Store config
        setConfig(config)
    }
    
    // Check blobs state
    let raphaelObj, size

    // Generate canvas
    const origin = [0,0] // [x,y]
    size = [config.viewBoxWidth,config.viewBoxHeight] // [width,height]
    raphaelObj = generateRaphaelObj(origin[0],origin[1],size[0],size[1])
    raphaelObj.canvas.setAttribute('style','overflow: hidden')

    
    // } else {
    //     // Otherwise, get the canvas object
    //     raphaelObj = getRaphaelObj()
    //     size = [raphaelObj.width,raphaelObj.height]
    // }

    // Generate blob
    const center = [size[0]/2,size[1]/2] // Set the center in the middle
    const baseRadius = size[0]/4 // Set the base radious to fit a circle half the length of the canvas
    const blob = createBlob(raphaelObj,config,center,baseRadius)
    setCurrentBlob(blob)
    
    // Store current canvas
    setRaphaelObj(raphaelObj)

    // Clear background and attach generated svg to main frame
    clearBg(config.targetEl)
    config.targetEl.appendChild(raphaelObj.canvas)

    // Set text values of fields
    document.getElementById("control-fill-text").innerText = document.getElementById("controls-fill").value
    document.getElementById("svg-code-p").innerText = getRaphaelObj().canvas.outerHTML
}

function suffleBlob() {
    clearBg(getConfig().targetEl)
    let config = updateConfig(getConfig())
    magicBlobs(config)
}

function saveBlob() {
    // Store current blob
    let blob = getCurrentBlob()
    let svgEl = getRaphaelObj().canvas.cloneNode(true)
    let svgElString = getRaphaelObj().canvas.outerHTML
    // svgEl.outerHTML = getRaphaelObj().canvas

    addBlob({ ...blob, svgElString })

    allocateBlob(svgEl)
    
}

function deleteSavedBlob(event) {

    let slotEl

    if(event.target.tagName === 'IMG'){
        slotEl = event.target.parentNode.parentNode
    } else if (event.target.tagName === 'BUTTON'){
        slotEl = event.target.parentNode
    }
    // Get slot index
    if(slotEl){
        let slotIndex = Array.from(slotEl.parentNode.children).indexOf(slotEl)
        
        deleteBlob(slotIndex)
        reAllocateBlobs()
    }
    
}

function reAllocateBlobs(){
    let blobs = getBlobs()

    // Set them as background in slots
    let slots = Array.from(document.getElementsByClassName('stored-blobs-container')[0].children)
    
    // Reset slots
    slots.forEach(slot => {
        // Clear slot
        let svgEl = slot.getElementsByTagName('svg')[0]
        if(svgEl){
            slot.removeChild(svgEl)
        }
        slot.classList.add('empty-slot')
        slot.classList.remove('small-blob-container')
    })

    // Set blob
    blobs.forEach((blob,i) => {
        // Restore blob from string
        let tmpDivEl = document.createElement('div') 
        tmpDivEl.innerHTML = blob.svgElString
        let blobEl = tmpDivEl.firstElementChild
        // document.removeChild(tmpDivEl)

        // Set blob
        slots[i].classList.remove('empty-slot')
        slots[i].classList.add('small-blob-container')
        slots[i].appendChild(blobEl)
    })
    
}


docReady(() => {
    
    restoreSession()

    let config = {
        "targetEl": document.getElementsByClassName('blob-container')[0],
        "irregularity": Number(document.getElementById('controls-irregularity').value),
        "numOfPoints": Number(document.getElementById('controls-num-points').value),
        "fill": document.getElementById('controls-fill').value,
        "stroke": Number(document.getElementById('controls-stroke').value),
        "viewBoxWidth": 200,
        "viewBoxHeight": 200,
    }

    magicBlobs(config)

    // Attach event listeners to inputs
    let inputs = Array.from(document.getElementsByTagName('input'))
    
    inputs.forEach(input => { 
        input.addEventListener('input', (ev) => {
            let config = updateConfig(ev.target.name,ev.target.value)
            magicBlobs(config)
        })
    })

})