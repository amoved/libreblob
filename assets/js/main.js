const PI = 3.1415

function docReady(fn) {
    // see if DOM is already available
    if (document.readyState === "complete" || document.readyState === "interactive") {
        // call on next available tick
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

function clearBg(container) {
    container.innerHTML= ''
}
  
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); // The maximum is exclusive and the minimum is inclusive
}

function generatePoint(center, radius){
    return [center[0] + getRandomInt(-radius,radius), center[1] + getRandomInt(-radius,radius)]
}

function mod(v) {
    return Math.sqrt(v[0]*v[0]+v[1]*v[1]) // Teorema de pitágoras
}

// function calculateSmoothB(start,end,deviationRadius) {
//     // Primero sacamos el vector director desde end hacia start, lo dos últimos puntos y su ángulo respecto al eje x
//     const v = [start[0]-end[0],start[1]-end[1]] // Resta el punto final menos el inicial
//     const u = [v[0]/mod(v),v[1]/mod(v)] // Dividimos entre el módulo para obtener el vector unitario
//     const alpha = Math.acos(u[0]) // cos(alpha) = (u * u_x) / (|u| * |u_x|) --> cos(alpha) = (u_0 * 1 + u_1 * 0) / (1*1)
//     console.log(`Point End = (${end[0].toFixed(0)},${end[1].toFixed(0)})`)
//     console.log(`Alpha = ${(alpha * 180/PI).toFixed(0)}`)

//     // Ahora calculamos el ángulo de B, que podrá hallarse en un rango de 
//     // grados desde el ángulo del vector director. Y calculamos el módulo
//     const minDeg = 30 * PI/180
//     const maxDeg = 100 * PI/180 // 90deg
//     const beta = alpha + ((Math.random() * (maxDeg - minDeg)) + minDeg); // The maximum is exclusive and the minimum is inclusive
//     const minMod = 0
//     const maxMod = deviationRadius 
//     const modB = Math.random() * (maxMod - minMod) + minMod
    
//     // Calculamos B, aplicando el vector incremento sobre el punto final
//     // B = (end_0 + |b| * cos(beta), end_1 + |b| * sen(beta))
//     const B = [end[0] + (modB * Math.cos(beta)), end[1] + (modB * Math.sin(beta))]   
//     console.log(`Point B = (${B[0].toFixed(0)},${B[1].toFixed(0)})`)
//     console.log(`Beta = ${(beta * 180/PI).toFixed(0)}`)
//     return [end[0] + (modB * Math.cos(beta)), end[1] + (modB * Math.sin(beta))]   
    
// }

// function generateClosedPath(points, irregularity, svgEl) {

//     console.log('------ New Path --------')

//     let angle = 0
//     const deltaAngle = 2*PI/points
//     const center = [50,50]
//     const maxDeviation = 20
//     const deviationRadius = maxDeviation*irregularity
//     const radius = 40
//     const initialPoint = [center[0] + radius, center[1]]
//     let path= `M ${initialPoint[0]},${initialPoint[1]}`
//     angle += deltaAngle
//     let prevPoint = initialPoint
//     drawCircle(svgEl,initialPoint[0],initialPoint[1],1)

//     path += ' R '

//     // Calculate intermidiate points
//     for(let i=0;i < points;i++){
//         const end = generatePoint([center[0] + radius * Math.cos(angle), center[1] + radius * Math.sin(angle)], deviationRadius)
//         drawCircle(svgEl,end[0],end[1],1,`rgba(0,0,0,${angle/(2*PI)})`)

        
//         // const b = calculateSmoothB(prevPoint,end,deviationRadius)
//         // drawCircle(svgEl,b[0],b[1],1,'lightblue')
//         // path += ` S ${b[0]} ${b[1]} ${end[0]} ${end[1]}`
//         path += `${end[0].toFixed(0)},${end[1].toFixed(0)} `

//         // path += ` C ${a[0]} ${a[1]} ${b[0]} ${b[1]} ${end[0]} ${end[1]}`
//         // path += ` L ${end[0]} ${end[1]}`
//         angle += deltaAngle
//         prevPoint = end
//     }

//     path += 'Z'
//     // const a = generatePoint(initialPoint,deviationRadius)
//     // const b = generatePoint(initialPoint,deviationRadius)
//     // const b = calculateSmoothB(prevPoint,initialPoint,deviationRadius)
//     // path += ` S ${b[0]} ${b[1]} ${initialPoint[0]} ${initialPoint[1]} z`
//     // path += ` C ${a[0]} ${a[1]} ${b[0]} ${b[1]} ${initialPoint[0]} ${initialPoint[1]}`
//     // path += ` L ${initialPoint[0]} ${initialPoint[1]}`

//     return path

// }

function rotatePoint(point, angle, center = [0,0]) {
    // console.log('Rotate point:')
    // console.log(point)
    // console.log('Angle:')
    // console.log(angle)
    let v = [point[0]-center[0],point[1]-center[1]]
    let module = mod(v)
    let alpha = Math.acos(v[0]/module)
    let rotatedV = [Math.round(module*Math.cos((angle*PI/180)+alpha)),Math.round(module*Math.sin((angle*PI/180)+alpha))]
    // console.log('Point rotated:')
    // console.log([rotatedV[0]+center[0],rotatedV[1]+center[1]])
    return [rotatedV[0]+center[0],rotatedV[1]+center[1]]
}

function getCirclePoints(nPoints,center,radius) {
    let angle = 0
    const deltaAngle = 2*PI/nPoints
    let points = []
    for(let i=0;i<nPoints;i++){
        points[i] = [center[0] + radius * Math.cos(angle), center[1] + radius * Math.sin(angle)]
        angle += deltaAngle
    }
    return points
}

function getRandomCirclePoints(nPoints,center,radius,deviation) {
    let angle = 0
    const deltaAngle = 2*PI/nPoints
    let points = []
    for(let i=0;i<nPoints;i++){
        circlePoint = [center[0] + radius * Math.cos(angle), center[1] + radius * Math.sin(angle)]
        points[i] = generatePoint(circlePoint, deviation)
        angle += deltaAngle
    }
    return points
}

function generateCatmutPathInput(center, numOfPoints, irregularity, radius) {

    let angle = 0
    let points = []
    const deltaAngle = 2*PI/numOfPoints
    const maxDeviation = 20
    const deviationRadius = maxDeviation*irregularity
    const initialPoint = [center[0] + radius, center[1]]
    points.push(initialPoint)
    let path= `M ${initialPoint[0]},${initialPoint[1]} R `
    angle += deltaAngle

    // Calculate intermidiate numOfPoints
    for(let i=1;i < numOfPoints;i++){
        const end = generatePoint([center[0] + radius * Math.cos(angle), center[1] + radius * Math.sin(angle)], deviationRadius)
        points.push(end)
        path += `${end[0].toFixed(0)},${end[1].toFixed(0)} `
        angle += deltaAngle
    }
    path += 'Z'

    return [points,path]
}

function generateCatmutPathInputFromPoints(points) {
    console.log('Points:')
    console.log(points)
    let path= `M ${points[0][0]},${points[0][1]} R `
    for(let i=1;i < points.length;i++){
        path += `${points[i][0].toFixed(0)},${points[i][1].toFixed(0)} `
    }
    path += 'Z'
    return path
}

function generateSVG({numOfBlobs,numOfPoints,irregularity,scale, stroke, fill}) {
    
    const x_min = 0
    const y_min = 0
    const x_max = 200
    const y_max = 200
    const viewBoxCenter = [x_max/2,y_max/2]
    const baseRadius = (x_max/4)
    let raphaelObj = new Raphael(x_min,y_min,x_max,y_max) 
    const centers = getRandomCirclePoints(numOfBlobs,viewBoxCenter,x_max/2,20) // number of numOfBlobs, center of the view box, radius, deviation
    const blobs = []

    for(let i=0;i<numOfBlobs;i++){

        let [points, catmutPathInput] = generateCatmutPathInput(centers[i], numOfPoints, irregularity, baseRadius * scale)
        
        // Generate Path 
        let pathEl = raphaelObj.path(catmutPathInput)

        // Set path HTML element attributes
        pathEl.node.setAttribute('stroke-linecap', 'round')
        pathEl.node.setAttribute('stroke-linejoin', 'round')
        pathEl.node.setAttribute('stroke-width', stroke)
        pathEl.node.setAttribute('fill', fill)

        blobs.push({
            catmutPathInput,
            center: centers[i],
            points: [...points],
            radius: baseRadius * scale,
            angle: 0,
        })
    
    }

    console.log('Computed blobs:')
    console.log(blobs)
    setRaphaelObj(raphaelObj)
    setBlobs(blobs)

    raphaelObj.canvas.setAttribute('viewBox',`${x_min} ${y_min} ${x_max} ${y_max}`)
    raphaelObj.canvas.removeAttribute('height')
    raphaelObj.canvas.removeAttribute('width')
    return raphaelObj.canvas
}

function rotateBlobs(angle){
    console.log("Received blobs")
    let blobs = getBlobs()
    console.log(blobs)

    console.log("Current blobs")
    console.log(document.magicBlobsState.blobs)
    let newBlobs = []
    blobs.forEach(blob => {
        console.log('Loaded blob')
        console.log(blob)
        console.log(blob.points)
        let newBlob = blob
        let newPoints = [] 
        blob.points.forEach(point => newPoints.push(rotatePoint(point,angle-blob.angle,blob.center))) 
        console.log('New points')
        console.log(newPoints)
        newBlob.points = newPoints
        newBlob.angle = angle
        console.log(newBlob.points)
        newBlob.catmutPathInput = generateCatmutPathInputFromPoints(newBlob.points)
        newBlobs.push(newBlob)
    });
    setBlobs(newBlobs)
    updateSVGPaths()
}

function updateSVGPaths() {
    let containerEl = document.getElementsByClassName('blob-container')[0]
    let raphaelObj = getRaphaelObj()
    let blobs = getBlobs()
    let config = getConfig()
    raphaelObj.clear()
    blobs.forEach(blob => {
        let pathEl = raphaelObj.path(blob.catmutPathInput)
        // Set path HTML element attributes
        pathEl.node.setAttribute('stroke-linecap', 'round')
        pathEl.node.setAttribute('stroke-linejoin', 'round')
        pathEl.node.setAttribute('stroke-width', config.stroke)
        pathEl.node.setAttribute('fill', config.fill)

    })
    containerEl.innerHTML = ''
    containerEl.appendChild(raphaelObj.canvas)
}

function magicBlobs(params) {

    if(!params.targetEl) {
        console.log("Specify the target element to add the background")
    }

    params.targetEl.classList.add('magicBlobBg')
   
    const svgEl = generateSVG(params)
    svgEl.style.transform = `scale(1.1) rotateZ(${params.rotate}deg)`
    svgEl.classList.add('blob')
    params.targetEl.appendChild(svgEl)

}

function getConfig() {
    return document.magicBlobsState.config
}

function getBlobs() {
    return document.magicBlobsState.blobs
}

function setBlobs(blobs) {
    console.log("Input blobs to store:")
    console.log(blobs)
    document.magicBlobsState.blobs = JSON.parse(JSON.stringify(blobs))
    console.log("Stored blobs:")
    console.log(document.magicBlobsState.blobs)
}

function getRaphaelObj() {
    return document.magicBlobsState.raphaelObj
}

function setRaphaelObj(obj) {
    // document.magicBlobsState.raphaelObj = JSON.parse(JSON.stringify(obj))
    document.magicBlobsState.raphaelObj = obj
    console.log("Stored raphäel obj:")
    console.log(document.magicBlobsState.raphaelObj)
}

function updateConfig(key,value) {
    switch(key){
        case 'fill':
            document.magicBlobsState.config[key] = value
            magicBlobs(config)
        break
        case 'rotate':
            document.magicBlobsState.config[key] = Number(value)
            rotateBlobs(value)
        break
        default:
            document.magicBlobsState.config[key] = Number(value)
            magicBlobs(config)
        break
    }
    console.log(document.magicBlobsState.config)
}

docReady(() => {
    
    let blobs = []
    let config = {
        "targetEl": document.getElementsByClassName('blob-container')[0],
        "numOfBlobs": Number(document.getElementById('controls-num-blobs').value),
        "scale": Number(document.getElementById('controls-scale').value),
        "rotate": Number(document.getElementById('controls-rotate').value),
        "sizeDeviation": Number(document.getElementById('controls-size-deviation').value),
        "irregularity": Number(document.getElementById('controls-irregularity').value),
        "numOfPoints": Number(document.getElementById('controls-num-points').value),
        "fill": document.getElementById('controls-fill').value,
        "stroke": Number(document.getElementById('controls-stroke').value)
    }

    document.magicBlobsState = {
        blobs,
        config,
        raphaelObj: {}
    }

    magicBlobs(config)

    let form = document.forms['blobs-controls']
    form.addEventListener('input', (ev) => {
        // if(ev.target.name == 'rotate'){
        //     let svgContainerEl = document.getElementsByClassName('blob-container')[0]
        //     svgContainerEl.style.transform = `scale(1.1) rotateZ(${Number(ev.target.value)}deg)`
        //     return
        // }
        clearBg(config.targetEl)
        updateConfig(ev.target.name,ev.target.value)
    })

})