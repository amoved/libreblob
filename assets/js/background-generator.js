function selectBlob(event) {

    // If slot is empty ignore
    if(event.target.classList.contains('empty-slot')){
        return
    }
    // Remove previous selection
    if(document.getElementsByClassName('selected-blob')[0]){
        document.getElementsByClassName('selected-blob')[0].classList.remove('selected-blob')
    }
    // Select new slot
    event.currentTarget.classList.add('selected-blob')
    
    // Set state to track if blob is on canvas
    let blobIndex = Array.from(event.currentTarget.parentNode.children).indexOf(event.currentTarget)
    let currentBlob = setCurrentBlobFromBlobs(blobIndex)

    let currentBg = getCurrentBackground(), config = getConfig()
    // If blob is not already on canvas
    if(!currentBlob.isOnCanvas){

        let pathEl
        
        // If background is empty, attach whole svg
        if(!currentBg.id) {
            // Clone blob as background canvas
            let svgEl = event.currentTarget.getElementsByTagName('svg')[0].cloneNode(true)
            pathEl= svgEl.getElementsByTagName('path')[0]
            // Add id to background element and save it
            svgEl.id = `background-svg-${Date.now()}`
            currentBg.id = svgEl.id
            setCurrentBackground(currentBg)
            // Append svg to background contianer
            config.targetEl.appendChild(svgEl)

        } else {
            
            // Clone blob path
            let svgEl = event.currentTarget.getElementsByTagName('svg')[0]
            pathEl= svgEl.getElementsByTagName('path')[0].cloneNode(true)
            
            // Append svg element to target
            document.getElementById(currentBg.id).appendChild(pathEl)
        }
        
        // Update current blob status 
        currentBlob.isOnCanvas = true
        currentBlob.pathElOnCanvasId = pathEl.id
        setCurrentBlob(currentBlob)
    }



    // Update controls values
    let inputs = Array.from(document.getElementsByTagName('input'))

    inputs.forEach(input => {
        if(!currentBlob.transform){
            input.value = config.defaultVals[input.name]
            // Set config values to default
            updateConfig(input.name,config.defaultVals[input.name])
        } else {
            input.value = currentBlob.transform[input.name]
        }
    })
}

function setBlobTransform(){

    let config = getConfig(), currentBlob = getCurrentBlob(), currentBg = getCurrentBackground()

    // Ignore if no blob selected
    if(!currentBlob.isOnCanvas){
        return
    }
//     blob.style = {
//         position: 'absolute',
//         top: config.yPos + '%',
//         left: config.xPos + '%',
//         width: '100%',
//         transform: `scale(${config.scale}) rotateZ(${config.rotate}deg) translate(-50%,-50%)`,
//         transformOrigin: '0% 0%'
//     }

//     // Attach styles to canvas element
//     Object.keys(blob.style).forEach(key => {
//         console.log(`Current value of ${key} is ${document.getElementById(blob.svgElOnCanvasId).style[key]}`)
//         console.log(`Set it to ${blob.style[key]}`)
//         document.getElementById(blob.svgElOnCanvasId).style[key] = blob.style[key]
//         console.log( document.getElementById(blob.svgElOnCanvasId).style[key])
//     })

//    console.log(document.getElementById(blob.svgElOnCanvasId))

//     // Create class string
//     blob.styleString = `
//     .blob-${config.selectedBlobIndex} {\n
//         position: absolute;\n
//         width: 100%;\n
//         position: absolute;\n
//         top: ${blob.style.top};\n
//         left: ${blob.style.left};\n
//         transform: ${blob.style.transform};\n
//         transform-origin: 0% 0%;\n
//     }`

    currentBlob.transform = {
        xPos: config.xPos,
        yPos: config.yPos,
        scale: config.scale,
        rotate: config.rotate,
        translate: `translate(${config.xPos} ${config.yPos})`,
        scale: `scale(${config.scale})`,
        rotate: `rotateZ(${config.rotate})`,
        attString: `translate(${config.xPos} ${config.yPos}) scale(${config.scale}) rotate(${config.rotate} ${config.viewBoxWidth/2} ${config.viewBoxHeight/2})`
    }

    // Add transform attribute to path element
    let pathEl = document.getElementById(currentBlob.pathElOnCanvasId)
    pathEl.setAttribute('transform',currentBlob.transform.attString)

    setCurrentBlob(currentBlob)

    // Copy svg in code modal
    document.getElementById("svg-code-p").innerText = document.getElementById(currentBg.id).outerHTML
}

docReady(() => {
    
    let config = restoreSession()

    config.targetEl = document.getElementsByClassName('background-container')[0],
    config = {
        ...config,
        "selectedBlobIndex": -1,
        "xPos": 0,
        "yPos": 0,
        "scale": 1,
        "rotate": 0, 
        "defaultVals": {
            "xPos": 0,
            "yPos": 0,
            "scale": 1,
            "rotate": 0
        }
    }

    // Is there a config set already?
    if(getConfig() !== {}){
        // Check params
        if(!config.targetEl) {
            console.log("Specify the target element to add the background")
        }
        // Store config
        setConfig(config)
    }

    // Event listener to handle bg orientation
    // let bgOrientationEl = document.getElementById('controls-bg-orientation')
    // bgOrientationEl.addEventListener('change', () => {
    //     let containerEl = document.getElementById('bg-generator-container')
    //     containerEl.classList.toggle('f-col')
    //     containerEl.classList.toggle('f-row')

    // })

    // Add event listeners with capture to slots
    let slots = Array.from(document.getElementsByClassName('stored-blobs-container')[0].children)
    slots.forEach(slot => {
        slot.addEventListener('click', event => selectBlob(event) , true) // use capture to avoid children triggering event
    });

    // Attach event listeners to inputs
    let inputs = Array.from(document.getElementsByTagName('input'))
    
    inputs.forEach(input => { 
        input.addEventListener('input', (ev) => {
            updateConfig(ev.target.name,ev.target.value)
            setBlobTransform()
        })
    })

})

