# Libreblob

[Libreblob](https://libreblob.amoved.es) is a blob and blob background generator. It is a FLOSS tool that will help you create great shapes and backgrounds and export them as SVG images!

![Libreblob screenshot](./assets/img/screenshot.png)

## Dependencies
[Raphäel Js](https://dmitrybaranovskiy.github.io/raphael)

## Acknowledgments
After multiple attemps to create smooth shapes with the svg path element, we found a
[stackoverflow entry about smooth paths in SVG](https://stackoverflow.com/questions/9121133/smooth-svg-path-connection) with [catmull-rom spline](https://en.wikipedia.org/wiki/Cubic_Hermite_spline#Catmull%E2%80%93Rom_spline). We use the proposed library to generate the blobs given a set of points.

The UI is inspired in [BlobMaker](https://www.blobmaker.app/)

Icons:
- Polygons: https://www.flaticon.com/authors/voysla
- Scale, blob, circle, save, share and dice icon: https://www.freepik.com/
- Stroke icon: https://www.flaticon.com/authors/bearicons
- Rotation icon: https://www.flaticon.com/authors/maniprasanth 
- Circles icon: https://www.flaticon.com/authors/icon-mania 
- Download icon: https://www.flaticon.com/authors/debi-alpa-nugraha 
- Code icon: https://www.flaticon.com/authors/kiranshastry
- Close icon: https://www.flaticon.com/authors/ariefstudio 
- Horizontal icon: https://www.flaticon.com/authors/catalin-fertu
- Landscape icon: https://www.flaticon.com/authors/payungkead 

## License

This software is licensed unde [GPLv3](./LICENSE.md)